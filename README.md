# Firefox Hardening

An easy guide to make your Firefox more private and secure.

Do you use GNU/Linux ? Try using [firejail](https://linuxconfig.org/protect-your-system-run-your-browser-in-firejail) to sandbox firefox (and other programs).

Português: siga as instruções em português [aqui](https://0xacab.org/caioau/firefox-hardening#portugu%C3%AAs-um-guia-f%C3%A1cil-para-tornar-seu-firefox-privado-e-seguro)

## Acknowledgments:

heavily inspired by [ffprofile.com](https://ffprofile.com/), also thanks to [privacytools.io](https://www.privacytools.io/), [ghacks user.js](https://github.com/ghacksuserjs/ghacks-user.js) and [pyllyukko user.js](https://github.com/pyllyukko/user.js)

This guide is composed in three parts:

1. [Installing user.js file](https://0xacab.org/caioau/firefox-hardening#installing-userjs-file)
2. [Installing privacy add-ons](https://0xacab.org/caioau/firefox-hardening#installing-privacy-add-ons)
3. [Changing the default search engine](https://0xacab.org/caioau/firefox-hardening#changing-the-default-search-engine)

## Installing user.js file:

1. Download the [user.js](https://0xacab.org/caioau/firefox-hardening/raw/master/user.js) (right click , save as).
2. Go to the **menu** (upper right).
3. Click **help**.
4. Click **Troubleshooting Information**.
5. finally click **Open Directory** under Profile Directory.

then copy the downloaded user.js there

## Installing privacy add-ons:

Click **add to Firefox** to install the following add-ons:

* [uBlock Origin: Block Ads and Trackers](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
* [Cookie AutoDelete: Automatically Delete Cookies](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/) Don't forget to click in the icon and enable Auto-clean. 
* [HTTPS Everywhere: Secure Connections](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
* [Decentraleyes: Block Content Delivery Networks](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)

The following add-ons are not so essential, but nice to have:

* [Firefox Multi-Account Containers:  lets you keep parts of your online life separated](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)
* If you absolute need to use google (which you totally would not) use this: [Google search link fix](https://addons.mozilla.org/en-US/firefox/addon/google-search-link-fix/)
* [Invidious Redirect: Redirects all YouTube links to Invidous (Private Front-End To YouTube)](https://addons.mozilla.org/en-US/firefox/addon/hooktube-redirect/) 

## Changing the default search engine:

1. Go to the **menu** (upper right).
2. Click **Preferences**.
3. Click **Search** (on the left).
4. Change default search engine to a  privacy respecting search engines like DuckDuckGo, StartPage, Qwant or searX (maybe scroll down and click find more search engines).

## Português: Um guia fácil para tornar seu Firefox privado e seguro:

Usa GNU/Linux ? Tente usar o [firejail](https://linuxconfig.org/protect-your-system-run-your-browser-in-firejail) para "enjaular" o firefox (e outros programas).

## Agradecimentos:

Muito inspirado por [ffprofile.com](https://ffprofile.com/), também muito obrigado para [privacytools.io](https://www.privacytools.io/), [ghacks user.js](https://github.com/ghacksuserjs/ghacks-user.js) e [pyllyukko user.js](https://github.com/pyllyukko/user.js)

Esse guia é composto por três partes: 

1. [Instalando o arquivo user.js](https://0xacab.org/caioau/firefox-hardening#instalando-o-arquivo-userjs)
2. [Instalando extensões para privacidade](https://0xacab.org/caioau/firefox-hardening#instalando-extens%C3%B5es-para-privacidade)
3. [Alterando o Mecanismo de pesquisa padrão:](https://0xacab.org/caioau/firefox-hardening#alterando-o-mecanismo-de-pesquisa-padr%C3%A3o)

## Instalando o arquivo user.js:

1. Baixe o [user.js](https://0xacab.org/caioau/firefox-hardening/raw/master/user.js) (clique com o botão direito, salvar como).
2. Abra o menus (canto superior direito).
3. Clique em **Ajuda**.
4. Clique em **Informações para resolver problemas**.
5. Por fim clique em **Abrir pasta** sob Diretório do perfil

então copie o arquivo user.js baixado ai.

## Instalando extensões para privacidade:

Clique em **Adicionar ao Firefox** para instalar as seguintes extensões:

* [uBlock Origin: Bloqueie propagandas e rastreadores](https://addons.mozilla.org/pt-BR/firefox/addon/ublock-origin/)
* [Cookie AutoDelete: Exclua automaticamente Cookies](https://addons.mozilla.org/pt-BR/firefox/addon/cookie-autodelete/) Não se esqueça de clicar no ícone dele e Ativar Limpeza Automática.
* [HTTPS Everywhere: Conexões seguras](https://addons.mozilla.org/pt-BR/firefox/addon/https-everywhere/)
* [Decentraleyes: Bloqueie Redes de distribuição de conteúdo](https://addons.mozilla.org/pt-BR/firefox/addon/decentraleyes/)

As extensões abaixo não são tão essenciais, mas são boas de ter:

* [Firefox Multi-Account Containers:  Permite compartimentalizar partes da sua vida digital](https://addons.mozilla.org/pt-BR/firefox/addon/multi-account-containers/)
* Se você absolutamente precisa usar o Google (que você totalmente não deveria) use isso: [Google search link fix](https://addons.mozilla.org/en-US/firefox/addon/google-search-link-fix/)
* [Invidious Redirect: Redireciona todos os links do Youtube para Inviduos (Interface do Youtube que respeita sua privacidade)](https://addons.mozilla.org/pt-BR/firefox/addon/hooktube-redirect/) 

## Alterando o Mecanismo de pesquisa padrão:

1. Abra o **menu** (canto superior direito).
2. Clique em **Preferencias**.
3. Clique em **Pesquisa** (a esquerda).
4. Mude o mecanismo de busca padrão para um que respeita sua privacidade como DuckDuckGo, StartPage, Qwant ou searX (talvez precise rolar até embaixo e clicar em **Procurar mais mecanismos de pesquisa**)